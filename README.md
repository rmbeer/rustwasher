# RustWasher Project

## Description

With the execution of a single command, it generates a set of compilation tools for other systems.

It only works from GNU/Linux.

## Instructions for use

Modify the variable 'MyPrefix' in all.sh to generate all the tools in that directory.
Then run all.sh.

In case of any failure or debugging. From the '#ALLMAIN' line, you can deactivate the steps that you no longer need by commenting on the lines.

## Target systems

* Windows
* Android (still in plan)

## For contributions

You can always contribute patches. In case of problems, report it right here. Also for any question.

