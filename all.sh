#!/bin/bash
# LICENCE CC-BY-NC BY RMBEER
#
# VARIABLES:
# MyPrefix - Change it to your own directory system
#
# FUNCTIONS:
# From Download function to later, you can mark the lines like a comment for skip each step. Use it for debugging.
#
_targets="i686-w64-mingw32 x86_64-w64-mingw32"
MainDir=`pwd`
MyPrefix=/opt/MV2/MINGW/usr

Download(){
	#gcc
	ciclo=1
	while [ $ciclo -eq 1 ];
	do
		wget 'https://ftp.gnu.org/gnu/gcc/gcc-10.2.0/gcc-10.2.0.tar.xz' -T 5 -c
		if [ $? -eq 0 ]; then
			ciclo=0
		fi
	done

	#crt/header/winpthreads
	ciclo=1
	while [ $ciclo -eq 1 ];
	do
		wget 'https://sourceforge.net/projects/mingw-w64/files/mingw-w64/mingw-w64-release/mingw-w64-v8.0.0.tar.bz2' -T 5 -c
		if [ $? -eq 0 ]; then
			ciclo=0
		fi
	done

	#binutils
	ciclo=1
	while [ $ciclo -eq 1 ];
	do
		wget https://ftp.gnu.org/gnu/binutils/binutils-2.35.1.tar.gz -T 5 -c
		if [ $? -eq 0 ]; then
			ciclo=0
		fi
	done

	#isl
	ciclo=1
	while [ $ciclo -eq 1 ];
	do
		wget http://isl.gforge.inria.fr/isl-0.21.tar.bz2 -T 5 -c
		if [ $? -eq 0 ]; then
			ciclo=0
		fi
	done
}


Instalar_Binutils(){
	cd binutils-2.35.1
	sed -i 's/install_to_$(INSTALL_DEST) //' libiberty/Makefile.in
	sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" libiberty/configure

	for _target in ${_targets}; do
		echo -e "\e[93;44m\e[2KBuilding ${_target} cross binutils\e[0m"
		mkdir -p ../binutils-${_target} && cd "../binutils-${_target}"
		../binutils-2.35.1/configure --prefix="${MyPrefix}" \
			--target=${_target} \
			--infodir="${MyPrefix}"/share/info/${_target} \
			--enable-lto --enable-plugins \
			--enable-deterministic-archives \
			--disable-multilib --disable-nls \
			--disable-werror
		make
	done

	for _target in ${_targets}; do
		echo -e "\e[93;44m\e[2KInstalling ${_target} cross binutils\e[0m"
		cd ../binutils-${_target}
		make install
	done
}

Instalar_Headers(){
	cd "${MainDir}/mingw-w64-v8.0.0"
  for _target in ${_targets}; do
    echo -e "\e[93;44m\e[2KConfiguring ${_target} headers\e[0m"
    mkdir -p ../headers-${_target} && cd ../headers-${_target}
    ../mingw-w64-v8.0.0/mingw-w64-headers/configure --prefix="${MyPrefix}"/${_target} --enable-sdk=all --host=${_target}
  done

  for _target in ${_targets}; do
    echo -e "\e[93;44m\e[2KInstalling ${_target} headers\e[0m"
    cd ../headers-${_target}
    make install
  done

  echo -e "\e[93;44m\e[2KInstalling MinGW-w64 licenses\e[0m"
  install -Dm644 ../mingw-w64-v8.0.0/COPYING.MinGW-w64/COPYING.MinGW-w64.txt "${MyPrefix}"/share/licenses/mingw-w64-headers/COPYING.MinGW-w64.txt
  install -Dm644 ../mingw-w64-v8.0.0/COPYING.MinGW-w64-runtime/COPYING.MinGW-w64-runtime.txt "${MyPrefix}"/share/licenses/mingw-w64-headers/COPYING.MinGW-w64-runtime.txt
  install -Dm644 ../mingw-w64-v8.0.0/mingw-w64-headers/ddk/readme.txt "${MyPrefix}"/share/licenses/mingw-w64-headers/ddk-readme.txt
}

Instalar_CRT(){
  cd "${MainDir}/mingw-w64-v8.0.0"
  for _target in ${_targets}; do
    echo -e "\e[93;44m\e[2KBuilding ${_target} CRT\e[0m"
    if [ ${_target} == "i686-w64-mingw32" ]; then
        _crt_configure_args="--disable-lib64 --enable-lib32"
    elif [ ${_target} == "x86_64-w64-mingw32" ]; then
        _crt_configure_args="--disable-lib32 --enable-lib64"
    fi
    mkdir -p ../crt-${_target} && cd ../crt-${_target}
		echo "../mingw-w64-v8.0.0/mingw-w64-crt/configure --prefix="${MyPrefix}"/${_target} --host=${_target} --enable-wildcard ${_crt_configure_args}"
    ../mingw-w64-v8.0.0/mingw-w64-crt/configure --prefix="${MyPrefix}"/${_target} --host=${_target} --enable-wildcard ${_crt_configure_args}
    make
  done

  for _target in ${_targets}; do
    echo -e "\e[93;44m\e[2KInstalling ${_target} crt\e[0m"
    cd ../crt-${_target}
    make install
  done
}

Instalar_winpthreads(){
	cd "${MainDir}/mingw-w64-v8.0.0"
  for _target in ${_targets}; do
    echo -e "\e[93;44m\e[2KBuilding ${_target} winpthreads.\e[0m"
    mkdir -p ../winpthreads-build-${_target} && cd ../winpthreads-build-${_target}
		echo "../mingw-w64-v8.0.0/mingw-w64-libraries/winpthreads/configure --prefix=\"${MyPrefix}\"/${_target} --with-sysroot=\"${MyPrefix}\"/${_target} --host=${_target} --enable-static --enable-shared"
    ../mingw-w64-v8.0.0/mingw-w64-libraries/winpthreads/configure --prefix="${MyPrefix}"/${_target} --with-sysroot="${MyPrefix}"/${_target} --host=${_target} --enable-static --enable-shared
    make
  done

  for _target in ${_targets}; do
    echo -e "\e[93;44m\e[2KInstalling ${_target} winpthreads.\e[0m"
    cd ../winpthreads-build-${_target}
    make install
  done
}

Instalar_gcc(){
	cd "${MainDir}"
  ln -sf gcc-10.2.0 gcc
  cd gcc
  sed -i 's|\-Werror||g' libbacktrace/configure
  ln -sf ../isl-0.21 isl

  for _arch in ${_targets}; do
		echo -e "\e[93;44m\e[2KCompiling ${_arch} GCC\e[0m"
    mkdir -p Build-${_arch} && pushd Build-${_arch}
    ../configure --prefix="${MyPrefix}" --libexecdir="${MyPrefix}"/lib \
        --target=${_arch} \
        --enable-languages=c,c++ \
        --enable-shared --enable-static \
        --enable-threads=posix --enable-fully-dynamic-string \
        --enable-libstdcxx-time=yes --enable-libstdcxx-filesystem-ts=yes \
        --with-system-zlib --enable-cloog-backend=isl \
        --enable-libgomp \
        --disable-multilib --enable-checking=release \
        --disable-sjlj-exceptions --with-dwarf2
    make
		popd
  done

  for _arch in ${_targets}; do
		echo -e "\e[93;44m\e[2KInstalling ${_arch} GCC\e[0m"
	  pushd Build-${_arch}
    make install
    #${_arch}-strip "${MyPrefix}"/${_arch}/lib/*.dll
    #strip "${MyPrefix}"/bin/${_arch}-*
    #strip "${MyPrefix}"/lib/gcc/${_arch}/10.2.0/{cc1*,collect2,gnat1,f951,lto*}
    ln -s ${_arch}-gcc "${MyPrefix}"/bin/${_arch}-cc
    # mv dlls
    #mkdir -p "${MyPrefix}"/${_arch}/bin/
    #mv "${MyPrefix}"/${_arch}/lib/*.dll "${MyPrefix}"/${_arch}/bin/
		popd
  done
}

#https://ftp.gnu.org/gnu/gcc/gcc-10.2.0/gcc-10.2.0.tar.xz
#https://sourceforge.net/projects/mingw-w64/files/mingw-w64/mingw-w64-release/mingw-w64-v8.0.0.tar.bz2
#https://ftp.gnu.org/gnu/binutils/binutils-2.35.1.tar.gz

#binutils: - (zlib)
#headers: -
#crt: binutils(m), gcc(m), headers(m)
#winpthreads: binutils(m), crt(m), gcc(m)
#gcc: binutils, crt, headers, winpthreads, gcc-ada(m) (zlib, libmpc)

#ALLMAIN

Download

tar Jxvf gcc-10.2.0.tar.xz
tar zxvf binutils-2.35.1.tar.gz
tar jxvf mingw-w64-v8.0.0.tar.bz2

Instalar_Binutils
Instalar_Headers
Instalar_CRT
Instalar_winpthreads
Instalar_gcc

